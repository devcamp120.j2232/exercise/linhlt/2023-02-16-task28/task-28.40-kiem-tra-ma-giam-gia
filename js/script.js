"use strict";
$(document).ready(function(){

  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gREADY_STATE_REQUEST_DONE = 4;
var gSTATUS_REQUEST_DONE = 200;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$("#btn-check-voucher").on("click",onBtnCheckVoucherClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm này để xử lý sự kiện nút check voucher click
function onBtnCheckVoucherClick(){
//subtask1
  var vInputVoucher = $("#voucher");//truy vấn ô mã giảm giá
  var vDivResultCheck = $("#div-result-check"); //truy vấn thẻ div
  console.log("Tôi lấy dữ liệu từ:");
  console.log("Input id = " +vInputVoucher.attr("id") + " , placeholder = " + vInputVoucher.attr("placeholder"));
  console.log("Tác động vào:");
  console.log("Div id = " + vDivResultCheck.attr("id") + " , innerHTML = " + vDivResultCheck.html());

  //khai báo đối tonwjg voucher
  var vVoucherObj = {
    maGiamGia:""
  }
  //b1: lấy giá trị nhập trên form
  var vVoucherCode = readData(vVoucherObj);
  
  // B2: Validate data
  var vIsValidateData = validateData(vVoucherObj);
  if(vIsValidateData) {
    // B3: Tạo request và gửi mã voucher về server
    var vXmlHttp = new XMLHttpRequest();
    sendVoucherToServer(vVoucherObj, vXmlHttp);
    // B4: xu ly response khi server trả về
    vXmlHttp.onreadystatechange = function() {
      if(vXmlHttp.readyState === gREADY_STATE_REQUEST_DONE
        && vXmlHttp.status === gSTATUS_REQUEST_DONE) {
          processResponse(vXmlHttp);
        }
        else if(vXmlHttp.readyState ==gSTATUS_REQUEST_DONE){
          var vDivResultCheck = $("#div-result-check");
          vDivResultCheck.html("Không tồn tại mã giảm giá");
        }
    }
  }
}
});


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm đọc dữ liệu từ ô text
function readData(paramVoucherObj){
  var vVoucherCodeElement = $("#voucher");
  var vVoucherCode = vVoucherCodeElement.val();
  paramVoucherObj.maGiamGia = vVoucherCode;
}


// Hàm này dùng để validate data
// input: ma voucher nguoi dung nhap vao
// output: tra ve true neu ma voucher duoc nhap, nguoc lai thi return false
function validateData(paramVoucherObj) {
  var vResultCheckElement = $("#div-result-check");
  if(paramVoucherObj.maGiamGia === "") {
    // Hiển thị câu thông báo lỗi ra
    vResultCheckElement.html("Mã giảm giá chưa nhập!");
    // Thay đổi class css, để đổi màu chữ thành màu đỏ
    vResultCheckElement.prop("class","text-danger");
    return false;
  }
  else{
  // Thay đổi class css, để đổi màu chữ thành màu đen bình thường
  vResultCheckElement.prop("class", "text-dark") ;
  vResultCheckElement.html("") ;
  return true;
  } 

}

// Ham thuc hien viec call api va gui ma voucher ve server
function sendVoucherToServer(paramVoucherObj, paramXmlVoucherRequest) {
  paramXmlVoucherRequest.open("GET", "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherObj.maGiamGia, true);
  paramXmlVoucherRequest.send();
}

// Hàm này được dùng để xử lý khi server trả về response
function processResponse(paramXmlHttp) {
  // B1: nhận lại response dạng JSON ở xmlHttp.responseText
  var vJsonVoucherResponse = paramXmlHttp.responseText;
  // B2: Parsing chuỗi JSON thành Object
  var vVoucherResObj = JSON.parse(vJsonVoucherResponse); 
  console.log(vJsonVoucherResponse);
  // B3: xử lý mã giảm giá dựa vào đối tượng vừa có
  var vDiscount = vVoucherResObj.phanTramGiamGia;
  var vResultCheckElement = $("#div-result-check");
  vResultCheckElement.html("Mã giảm giá " + vDiscount +"%"); 
   
}